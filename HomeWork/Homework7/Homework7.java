/*
На вход подается последовательность чисел, оканчивающихся на -1.

Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.

Гарантируется:

Все числа в диапазоне от -100 до 100.

Числа встречаются не более 2 147 483 647-раз каждое.

Сложность алгоритма - O(n)

 */

package Homework7;

import java.util.HashMap;
import java.util.Map;

public class Homework7 {
    public static void main(String[] args) {
        int[] a = {4 ,4 ,2 ,1 ,1 ,5 ,6 ,6 ,2 ,2};
        Map<Integer, Integer > map = new HashMap<>();
        for (int i : a)  {
            map.merge(i,1,Integer::sum);

        }
        int minCount = Integer.MAX_VALUE;
        int minNamber = -1;
        for(Map.Entry<Integer,Integer> entry : map.entrySet()) {
            int count = entry.getValue();

            if (count < minCount){
                minCount = count;
                minNamber = entry.getKey();
            }

        }
        System.out.println("Число которое встретилось мин кол-во раз:" + minNamber);
    }
}
