package Homework9;

public class Homework9 {
    public static void main(String[] args) {
        Figure figure = new Figure(10,4);
        Rectangle rectangle =new Rectangle(0,0,10,10);
        Ellipse ellipse = new Ellipse(0,0,5,4);
        Circle circle = new Circle(0,0,5);
        Square square = new Square(0,0,12);
          System.out.println("Периметр прямоугольника = " + rectangle.getPerimeter());
          System.out.println("Периметр эллипса = " + ellipse.getPerimeter());
          System.out.println("Периметр круга = " + circle.getPerimeter());
          System.out.println("Периметр квадрата = " + square.getPerimeter());
          System.out.println(figure.getPerimeter());



    }
}
