package Homework9;

public class Circle extends Ellipse {
    double radius;

    public Circle(double x, double y,double radius) {
        super(x, y, radius, radius);
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {

        return radius * (2 * Math.PI);
    }

}
