package Homework9;

public class Square extends Rectangle {
    double c ;

    public Square(double x, double y, double c) {
        super(x, y, c, c);
        this.c = c;
    }

    @Override
    public double getPerimeter() {
        return c * 4;
    }
}
