package Homework9;

public class Rectangle extends Figure {
    protected double a;
    private double b;

    public Rectangle(double x, double y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return a * 2 + b * 2;
    }
}
