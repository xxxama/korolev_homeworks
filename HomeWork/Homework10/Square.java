package Homework10;

public class Square extends Rectangle implements Movable {
    double c ;

    public Square(double x, double y, double c) {
        super(x, y, c, c);
        this.c = c;
    }

    @Override
    public double getPerimeter() {
        return c * 4;
    }

    @Override
    public void move(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
