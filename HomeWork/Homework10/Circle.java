package Homework10;

public class Circle extends Ellipse  implements Movable {
    double radius;


    public Circle(double x, double y,double radius) {
        super(x, y, radius, radius);
        this.radius = radius;
    }


    @Override
    public double getPerimeter() {

        return radius * (2 * Math.PI);
    }

    @Override
    public void move(double x, double y) {
        this.x = x;
        this.y = y;
        System.out.println("Смещение" + getX() + " " + getY());


    }
}
