/*
Сделать класс Figure из задания 09 абстрактным.
Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.

 */

package Homework10;

import java.util.Arrays;

public class Homework10 {
    public static void main(String[] args) {
    Movable[] figure = new Movable[2];
    figure[0] = new Square(0,0,5);
    figure[1] = new Circle(0,0,2);

        Arrays.stream(figure).forEach(figures -> System.out.println(((Figure) figures).getCoordinates()));
        Arrays.stream(figure).forEach(figures -> figures.move(10, 15));
        Arrays.stream(figure).forEach(figures -> System.out.println(((Figure) figures).getCoordinates()));



    }
}
