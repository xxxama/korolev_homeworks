package Homework8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Homework8 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        People[] people = new People[2];
          for (int i = 0; i < people.length; i++) {
             System.out.println("Введите имя");
             String name = scanner.next();
              System.out.println("Введите вес");
            double Weight = scanner.nextDouble();
              if (Weight < 0 ) {
                  System.out.println("Вееден вес меньше 0");
              }
              people[i] = new People(name,Weight);

        }
        Arrays.sort(people, Comparator.comparing(People :: getWeight));
        System.out.println(Arrays.toString(people));
    }

}
