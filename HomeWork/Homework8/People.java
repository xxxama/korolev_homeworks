package Homework8;

public class People {
    private String Name;
    private double Weight;

    public People(String name, double weight) {
        this.Name = name;
        this.Weight = weight;
    }

    public double getWeight() {
        return this.Weight;
    }
    @Override
    public String toString() {
        return "People{" +
                "Name='" + Name + '\'' +
                ", Weight=" + Weight +
                '}';
    }


}
