/*
Реализовать функцию, принимающую на вход массив и целое число.
Данная функция должна вернуть индекс этого числа в массиве.
Если число в массиве отсутствует - вернуть -1.
Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
стало
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0

 */


package Homework6;
import java.util.Arrays;
import java.util.Scanner;

public class Homework6 {
    public static void main(String[] args) {
        int[] a = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        arrayElementToLeft(a);
        System.out.println(Arrays.toString(a));
        int d = indexOf(a);
        System.out.println("Введенное число находиться под индексом:" +  d);
    }

    static int indexOf ( int[] array){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        int h = scanner.nextInt();
        int n = h;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                return i;
            }
        }
        return -1;
    }

    static void arrayElementToLeft (int [] array){
        int indexOfZero = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0){
                if (indexOfZero < i ) {
                    array[indexOfZero] = array[i];
                    array[i] = 0;
                }

                indexOfZero++;
            }
        }
    }
}
