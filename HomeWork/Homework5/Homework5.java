/*
Реализовать программу на Java, которая для последовательности чисел,
оканчивающихся на -1 выведет самую минимальную цифру,
встречающуюся среди чисел последовательности.
 */

package Homework5;
import java.util.Scanner;

public class Homework5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int l = a;
        int n;
        int digit;
        int minDigit = a;
        while (a != -1) {
            System.out.println("Введит новое число");
            a = scanner.nextInt();
            while (a > 0) {
                digit = a % 10;
                a /= 10;
                n = digit;
                if (l > n) {
                    l = n;
                }
                if (minDigit > l) {
                    minDigit = l;
                }
            }
        }
        System.out.println("Минимальная цифра :" + minDigit);
    }
}
